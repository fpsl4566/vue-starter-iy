import {useAuthStore} from "~/stores/testauth"; //인포트 처리 하기

export default defineNuxtRouteMiddleware((to) => {
    const { authenticated } = storeToRefs(useAuthStore()); // make authenticated state reactive
    const token = useCookie('token'); // get token from cookies

    // { } 감싸져 있는 이유
    //ref 참조해서   가져와라

    if (token.value) {
        // check if value exists
        authenticated.value = true; // update the state to authenticated
    }

    // if token exists and url is /login redirect to homepage
    if (token.value && to?.name === 'login') {
        return navigateTo('/');
    }
    // ? 있을수도 없을수도 ts어 있는데 이름이 로그인이면 to?(현재 페이지 이름)
    // 미들웨어가 껴들어서 가로채다
    // 토큰이 있는데 로그인 접근 x
    //index 보낸다
    // if token doesn't exist redirect to log in

    if (!token.value && to?.name !== 'login') {
        abortNavigation();
        return navigateTo('/login');
        //로그인 페이지로 보낸다
        //미들웨어가 처리
    }
});