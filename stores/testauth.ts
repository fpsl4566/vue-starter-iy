import { defineStore } from 'pinia';

interface UserPayloadInterface {
    username: string;
    password: string;
}

export const useAuthStore = defineStore('auth', {
    state: () => ({
        authenticated: false, //하나만 받고 싶은데 로딩까지 받는다?
        loading: false,
    }),
    actions: {
        async authenticateUser({ username, password }: UserPayloadInterface) {
            // useFetch from nuxt 3
            const { data, pending }: any = await useFetch('http://192.168.0.151:8080/v1/login/web/admin', {
                method: 'post',
                // headers: { 'Content-Type': 'application/json' }, 넣을 필요 없다 자동으로 붙는다.
                body: {
                    username,
                    password,
                },
                //username, password 받을거다.
            });
            this.loading = pending;

            // 성공하면 쿠키에 전화 걸어서 응답 결과 데이터에
            //data,pending 공유하고 합친 다.

            if (data.value) {
                const token = useCookie('token'); // useCookie new hook in nuxt 3
                token.value = data?.value?.token; // set token to cookie
                this.authenticated = true; // set authenticated  state value to true
            }
            //api가 넘겨준 토큰 값 쿠기에 넣는다.
            //인증이 됐는지 알려준다.
        },
        logUserOut() {
            const token = useCookie('token'); // useCookie new hook in nuxt 3
            this.authenticated = false; // set authenticated  state value to false
            token.value = null; // clear the token cookie
        },
        // 로그아웃 행위 버린다.
        //인증 상태를 인증으로 변경한다.
    },
});